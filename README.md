# Ejercicio Route1

Refer to the exhibits. All three of the conditions for the Choice router are true. What messages are written in the application log?

![alt text](image.png)

```
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core"
	xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">
	<http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="b098ca98-1f9b-4173-9b21-db8835bf001e" >
		<http:listener-connection host="0.0.0.0" port="8081" />
	</http:listener-config>
	<flow name="ejercicio_route1Flow" doc:id="b89e523f-38d7-4415-b6a2-9e40f957558b" >
		<http:listener doc:name="HTTP: GET /" doc:id="c792ce7d-52aa-4ac8-85d0-4540ad2062cc" config-ref="HTTP_Listener_config" path="/practica"/>
		<choice doc:name="Choice" doc:id="15852df7-d829-4e1f-8df4-3b70a94655e6" >
			<when expression="true">
				<logger level="INFO" doc:name="Route 1" doc:id="9c41ce86-4622-43bf-b328-6beeaec98923" message='#["Route 1"]'/>
			</when>
			<when expression="true">
				<logger level="INFO" doc:name="Route 2" doc:id="151fb65f-6d2b-4700-a82a-b2db22227c5a" message='#["Route 1"]'/>
			</when>
			<otherwise >
				<logger level="INFO" doc:name="Default" doc:id="62ebbe13-a30b-415a-ac66-1f8f1b8cc365" message='#["Default"]'/>
			</otherwise>
		</choice>
	</flow>
</mule>
```
- [x] Route1
- [ ] Route2
- [ ] Route1,Route2
- [ ] Route1,Route2,Default

**Respuesta correcta:** Route1